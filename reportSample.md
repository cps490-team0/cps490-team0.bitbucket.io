# README.md - CPS490 Report Template

University of Dayton

Department of Computer Science

CPS 490 - Capstone I, Semester Year

Instructor: Dr. Phu Phung


## Capstone I Project 


# The Messenger Application


# Team members

1.  Phu Phung, phu@udayton.edu
2.  Anna Duricy, duricya1@udayton.edu


# Project Management Information

Management board (private access): <https://trello.com/b/6vRUKUsR/cps490-f20>

Source code repository (private access): <https://bitbucket.org/cps490-team0/>


## Revision History

| Date     |   Version     |  Description |
|----------|:-------------:|-------------:|
|09/10/2020|  0.0          |   Sprint 0   |
|10/02/2020|  0.1          |   Sprint 1   |
|10/27/2020|  0.2          |   Sprint 2   |


# Overview

![System overview](imgs/overview-sample.png)

# System Analysis

Sprint 1

Our system consists of a backend JavaScript webserver and a front-end HTML client interface, deployed on a Google CloudShell server. The user can log into the system under their prefered name. The user can then send and receive real-time messages in a public chat or private chat environment.

Sprint 2

The user can now register for an account, with the registration being saved to a MongoDB database. The user can now create group chats.

## User Requirements

* Public chat
  * Users can send and receive messages in a public chatroom.

* Private chat
  * Users can send and receive messages to one other other privately.

* Group chat
  * Users can send and receive messages in a designated group chatroom.

* Registration
  * Users can register for an account.
  * User registration will be saved to a database.

* Login
  * Users can login to their registered account.

* Logout
  * Users can log out of their account.

* Message history
  * Users should be able to view their message history.
  * Message history will be saved to a database.

* Delete messages
  * Users should be able to delete messages that they have sent.
  * The messages will be deleted from the database.

## Use cases

![Sample use case](imgs/use-case.png)

# System Design

Sprint 1

A user who is not logged in has access to the login up. Once logged in, the user is directed to the chat UI so he or she can begin sending messages. The backend JavaScript sends and receives messages in real time, while the front-end HTML code dynamically updates the on-screen messages.

Sprint 2

A user who is not logged in has access to the login and register pages. Once the user has registered and logged in, he is directed to the chat UI. The user has separate GUIs for public, private, and group chat, to stay organized.


## Use-Case Realization

* Public chat: Completed Sprint 1

* Private chat: Completed Sprint 1

* Group chat: Completed Sprint 2

* Registration: Completed Sprint 1

* Login: Completed Sprint 1

* Logout: Completed Sprint 1

* Message history: Completed Sprint 2

* Delete messages: Completed Sprint 2

## Database 

Our database is setup on MongoDB.

Tables:

* users
  * Stores the usernames and passwords of user accounts.
  
* groupChat
  * Stores the members of every group chat
  
* messages
  * Stores all of the messages sent and the user or chatroom that they were sent to.

## User Interface

Sprint 1

When logged out, the user interface consists of a login box. Once the user has logged in, the user has access to different UIs for public chat and private chat. The user also has the button to log out.

Sprint 2

A logged out user has access to a UI for registration and logging in. Once logged in, the user has a public chat UI, private chat UI, and group chat UI. There should be a small button next to each message displaying the option to delete the message. The user also has a button to log out.

# Implementation

Sprint 1

All of the code is deployed on Google CloudShell.

UI: The UI is designed using CSS.

Login (client-side): An HTML input box allows the user to enter their username. The user then clicks a button to log in.

Login (serverside): JavaScript reads the input from the HTML box and logs the user in with their desired username.

Send message (client-side): The user types a message into an input box. The user then clicks a button to send the message. This can happen either in the private char or public chat UI.

Public message (serverside): The system receives the message, determines whether it was sent in private chat or public chat, and displays it to the correct recipients.

Sprint 2

Registration (client-side): The user enters their desired username and password to input boxes.

Registration (serverside): The username and password are checked to be unique, the password is hashed, and the username-hashed password pair are stored in the MongoDB database.


## Deployment

The code is deployed on Google CloudShell. Heroku hosts the site so users can access it from any web browser.

# Software Process Management

![Team Trello board](imgs/trello.png)

## Scrum process

### Sprint 0

Duration: 08/27/2020-09/10/2020

#### Completed Tasks: 

1. Team formation
2. Create Google CloudShell and Bitbucket
3. Create requirements

#### Contributions: 

1. Phu Phung, 5 hours, contributed in team formation, Bitbucket, requirements
2. Anna Duricy, 5 hours, contributed in team formation, Google CloudShell, requirements

#### Sprint Retrospective:

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
| Successfully found a team and created the necessary accounts |   |  |

### Sprint 1

Duration: 09/10/2020-10/05/2020

#### Completed Tasks: 

1. Public Chat
2. Private Chat
3. Login

#### Contributions: 

1. Phu Phung, 10 hours, contributed in public chat, private chat, and documentation
2. Anna Duricy, 11 hours, contributed in private chat, login, and documentation


#### Sprint Retrospective:

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
| Teamwork |  Pushing and pulling code   | Push smaller amounts of code and pull more often |


# User guide/Demo

Write as a demo with screenshots and as a guide for users to use your system.

_(Start from Sprint 1, keep updating)_
